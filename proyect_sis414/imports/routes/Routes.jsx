import Signin from '/imports/view/auth/Signin'
import Layout from '/imports/view/layout/Layout'
import {Home,Home2} from '/imports/view/home/Home'

const routes = [//un array de objectos donde estan mis rutas con mis componentes que quiero que renderice 
    {
        path:'/login',//ruta
        component:Signin
    },
    {
        path:'/dashboard',
        component:Layout,
        routes:[
            {
                path:"/dashboard/home",
                component:Home
            },
            {
                path:"/dashboard/home2",
                component:Home2
            },
        ]
    }
]

export default routes